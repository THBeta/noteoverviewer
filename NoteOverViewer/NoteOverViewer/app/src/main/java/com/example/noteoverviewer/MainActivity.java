package com.example.noteoverviewer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private static final String FILE_NAME = "list.json";
    private ListView listView;

    private ArrayList<Notiz> list = new ArrayList<Notiz>();
    private ArrayList<Integer>  posList = new ArrayList<Integer>();
    private NotizListAdapter adapter;
    private Button saveBut;
    private JSONArray jsonArray;
    private Context context;
    private TextView warning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // createNotificationChannel();
        listView = findViewById(R.id.listView);
        saveBut = findViewById(R.id.saveButt);
        warning = findViewById(R.id.warningField);


       /* try{
            JSONObject obj = new JSONObject(LoadJsonFromAsset());
            JSONArray array = obj.getJSONArray("list");
            for(int i = 0; i < array.length(); i++){
                JSONObject o = array.getJSONObject(i);
                Notiz notiz = gson.fromJson(String.valueOf(o), Notiz.class);
               list.add(notiz);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }*/


        Notiz n = new Notiz("MOMO", "momo", "Egal");
        Notiz m = new Notiz("LALA", "Test", "");
        Notiz x = new Notiz("Liese", "Test momomomomomo", "Dumm");
        Notiz y = new Notiz("Bot", "Test bototott", "wichtig");

        list.add(n);
        list.add(m);
        list.add(x);
        list.add(y);
        list.add(n);
        list.add(n);


        adapter = new NotizListAdapter(this, R.layout.adapter_view_layout, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                warning.setText("");
                if(posList.size() < 1  ){
                    posList.add(i);
                    view.setBackgroundColor(Color.YELLOW);
                } else if (posList.get(0) == i){
                    view.setBackgroundColor(Color.WHITE);
                    posList.remove(0);
                }

               /* Notiz notiz = list.get(i);
                Intent intent = new Intent(MainActivity.this, ViewNotiz.class);
                intent.putExtra("titel", notiz.getTitel());
                intent.putExtra("inhalt", notiz.getInhalt());
                intent.putExtra("bemerkung", notiz.getBemerkung());
                startActivity(intent);*/
            }
        });



        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {


                Intent intent = new Intent(MainActivity.this, EditNotiz.class);
                intent.putExtra("editNotiz", list.get(i));
                intent.putExtra("pos", Integer.toString(i));
                startActivityForResult(intent, 2);

                return true;
            }


        });




        NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, "titan")
                .setSmallIcon(R.drawable.ic_baseline_message_24)
                .setContentTitle("Speicherung erfolgreich")
                .setContentText("Das JSON File wurde gespeichert.")
                .setAutoCancel(true);


      /*NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

      saveBut.setOnClickListener( v -> {

          notificationManager.notify(100, builder.build());
      });*/

    }
/*
    public void createNotificationChannel(){

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.0)  {

            CharSequence name = "SaveChannel";
            String description = "für speicherung";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("titan", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }*/

    public String LoadJsonFromAsset() {
        String json = null;
        try{

            InputStream in = this.getAssets().open(FILE_NAME);
            int size = in.available();
            byte[] bbuffer = new byte[size];
            in.read(bbuffer);
            in.close();

            json = new String(bbuffer, "UTF-8");

        } catch (IOException e){
            e.printStackTrace();
            return null;
        }
        return json;
    }


    public void addNotiz(View view) {
        Intent intent = new Intent(this, CreateNotiz.class);
        startActivityForResult(intent, 1);

    }


    public void viewNotiz(View view){
        if(posList.size() == 1){
            Notiz notiz = list.get(posList.get(0));
            Intent intent = new Intent(MainActivity.this, ViewNotiz.class);
            intent.putExtra("titel", notiz.getTitel());
            intent.putExtra("inhalt", notiz.getInhalt());
            intent.putExtra("bemerkung", notiz.getBemerkung());
            startActivity(intent);
            posList.clear();
        } else {
            warning.setText("Keine Notiz ausgewählt");
        }


    }

    public void deleteItem(View view){
        if(posList.size() == 1){
            int i = posList.get(0);
            list.remove(list.get(i));
            posList.clear();
            adapter.notifyDataSetChanged();
        } else {
            warning.setText("Keine Notiz ausgewählt");
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Notiz notiz;
        Notiz original;
        int i;

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                notiz = data.getParcelableExtra("result");
                list.add(notiz);
                adapter.notifyDataSetChanged();

            }
        }

        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {

                notiz = data.getParcelableExtra("sendNotiz");
                i = Integer.parseInt(data.getStringExtra("position"));
                original = list.get(i);
                original.setTitel(notiz.getTitel());
                original.setInhalt(notiz.getInhalt());
                original.setBemerkung(notiz.getBemerkung());

                adapter.notifyDataSetChanged();

            }
        }
    }


}