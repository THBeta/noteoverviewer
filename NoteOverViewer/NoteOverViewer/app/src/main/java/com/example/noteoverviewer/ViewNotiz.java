package com.example.noteoverviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ViewNotiz extends AppCompatActivity {

    private TextView titelView;
    private TextView inhaltView;
    private TextView bemerkungView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_notiz);

        Intent intent = getIntent();
        String titel = intent.getStringExtra("titel");
        String inhalt = intent.getStringExtra("inhalt");
        String bemerkung = intent.getStringExtra("bemerkung");


        titelView = findViewById(R.id.titelView);
        inhaltView = findViewById(R.id.inhaltView);
        bemerkungView = findViewById(R.id.bemerkungView);

        titelView.setText(titel);
        inhaltView.setText(inhalt);
        bemerkungView.setText(bemerkung);
    }

    public void cancelView(View view){

        finish();
    }
}