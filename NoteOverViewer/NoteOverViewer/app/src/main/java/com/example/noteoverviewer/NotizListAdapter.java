package com.example.noteoverviewer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class NotizListAdapter extends ArrayAdapter<Notiz> {

    private static final String TAG = "NotizListAdapter";

    private Context mContext;
    private int mResource;
    private TextView tvTitel;
    private TextView tvBemerkung;

    public NotizListAdapter(Context context, int resource, ArrayList<Notiz> list) {
        super(context, resource, list);
        mContext = context;
        mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String titel = getItem(position).getTitel();
        String inhalt = getItem(position).getInhalt();
        String bemerkung = getItem(position).getBemerkung();

        Notiz notiz = new Notiz(titel, inhalt, bemerkung);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView tvTitel= (TextView) convertView.findViewById(R.id.titel);
        TextView tvBemerkung = (TextView) convertView.findViewById(R.id.bemerkung);

        tvTitel.setText(titel);
        tvBemerkung.setText(bemerkung);

        return convertView;
    }


}
