package com.example.noteoverviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EditNotiz extends AppCompatActivity {

    private TextView titelView;
    private TextView inhaltView;
    private TextView bemerkungView;

    private Notiz notiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_notiz);

        Intent intent = getIntent();
        notiz = intent.getParcelableExtra("editNotiz");
        int i = Integer.parseInt(intent.getStringExtra("pos"));
        System.out.println(i);

        titelView = (TextView) findViewById(R.id.editTitel);
        inhaltView = (TextView) findViewById(R.id.editInhalt);
        bemerkungView = (TextView) findViewById(R.id.editBemerkung);

        titelView.setText(notiz.getTitel());
        inhaltView.setText(notiz.getInhalt());
        bemerkungView.setText(notiz.getBemerkung());


    }


    public void editNotiz(View view){

        Intent idPos = getIntent();
        Intent editedNotiz = new Intent();
        Notiz notiz = new Notiz(titelView.getText().toString(), inhaltView.getText().toString(), bemerkungView.getText().toString());
        editedNotiz.putExtra("sendNotiz", notiz);
        editedNotiz.putExtra("position", idPos.getStringExtra("pos"));


        setResult(RESULT_OK, editedNotiz);
        finish();

    }

    public void cancelEdit(View view){

        finish();
    }
}