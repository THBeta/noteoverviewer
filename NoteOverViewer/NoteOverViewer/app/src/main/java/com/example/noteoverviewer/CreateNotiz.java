package com.example.noteoverviewer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CreateNotiz extends AppCompatActivity {

    private TextView titelView;
    private TextView inhaltView;
    private TextView bemerkungView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_notiz);

        titelView = (TextView) findViewById(R.id.newTitel);
        inhaltView = (TextView) findViewById(R.id.newInhalt);
        bemerkungView = (TextView) findViewById(R.id.newBemerkung);

    }


    public void newNotiz(View view){
        Intent resultNotiz = new Intent();
        Notiz notiz = new Notiz(titelView.getText().toString(), inhaltView.getText().toString(), bemerkungView.getText().toString());
        resultNotiz.putExtra("result", notiz);

        setResult(RESULT_OK, resultNotiz);
        finish();
    }

    public void cancelCreate(View view){

        finish();
    }
}