package com.example.noteoverviewer;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class Notiz implements Parcelable {

    private String titel;
    private String inhalt;
    private String bemerkung;


    public Notiz(String titel, String inhalt, String bemerkung) {
        this.titel = titel;
        this.inhalt = inhalt;
        this.bemerkung = bemerkung;
    }


    protected Notiz(Parcel in) {
        titel = in.readString();
        inhalt = in.readString();
        bemerkung = in.readString();
    }

    public static final Creator<Notiz> CREATOR = new Creator<Notiz>() {
        @Override
        public Notiz createFromParcel(Parcel in) {
            return new Notiz(in);
        }

        @Override
        public Notiz[] newArray(int size) {
            return new Notiz[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(titel);
        parcel.writeString(inhalt);
        parcel.writeString(bemerkung);
    }

    public JSONObject toJSON() throws JSONException {

        JSONObject jo = new JSONObject();
        jo.put("string", titel);
        jo.put("string", inhalt);
        jo.put("string", bemerkung);


        return jo;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }


}
